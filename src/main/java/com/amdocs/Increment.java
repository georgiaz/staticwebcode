package com.amdocs;

public class Increment {
	
	private int counter = 1;
	private int abc;
	  
	public Increment()
	{
		abc= 0;
	}

	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(final int input) {
		if (input == 0){
	        abc= counter--;
		}
		else {
			if (input == 1) {
				abc= counter;
			}
			else
			{
				abc= counter++;
			}
		}
		return abc;
	}			
}
